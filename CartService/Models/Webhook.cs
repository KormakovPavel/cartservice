﻿
namespace CartService
{
    public class Webhook
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Content { get; set; }
    }
}
