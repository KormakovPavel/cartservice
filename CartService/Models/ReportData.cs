﻿
namespace CartService
{
    public class ReportData
    {
        public int CountCarts { get; set; }
        public int CountCartsWithProductBonus { get; set; }
        public int CountCartDeadLine10Day { get; set; }
        public int CountCartDeadLine20Day { get; set; }
        public int CountCartDeadLine30Day { get; set; }
        public int AvgCostCart { get; set; }       
    }
}
