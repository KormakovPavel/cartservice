﻿
namespace CartService
{
    public class Setting
    {          
        public string ConnectionString { get; set; }
        public string ConnectionHangfire { get; set; }
        public string PathName { get; set; }      
        public string CronReport { get; set; }
        public string CronClear { get; set; }
    }
}
