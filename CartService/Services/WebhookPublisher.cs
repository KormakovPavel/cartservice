﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace CartService
{
    public class WebhookPublisher : IWebhookPublisher
    {
        private readonly IWebhookRepository _webhookRepository;
        public List<string> ErrorMessages { get; set; }
        public WebhookPublisher(IWebhookRepository webhookRepository)
        {
            _webhookRepository = webhookRepository;
            ErrorMessages = new List<string>();
        }

        public async Task<bool> Send()
        {
            var webhooks = await _webhookRepository.GetWebhooks();
            
            foreach (var webhook in webhooks)
            {
                using var httpClient = new HttpClient();                
                var result = await httpClient.PostAsync(webhook.Url,new StringContent(webhook.Content));
                if(result.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    ErrorMessages.Add(await result.RequestMessage.Content.ReadAsStringAsync());
                    return false;
                }
            }

            return true;
        }
    }
}
