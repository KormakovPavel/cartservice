﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Hangfire;
using Hangfire.Server;

namespace CartService
{
    internal class RecurringJobsService : BackgroundService
    {
        private readonly Setting _setting;
        private readonly IRecurringJobManager _recurringJobs;
        private readonly ILogger<RecurringJobScheduler> _logger;        
        private readonly IReport _report;
        private readonly ICartRepository _cartRepository;
        public RecurringJobsService(IOptions<Setting> options, IRecurringJobManager recurringJobs, ILogger<RecurringJobScheduler> logger,
                                    IReport report, ICartRepository cartRepository)
        {
            _setting = options.Value;
            _recurringJobs = recurringJobs;
            _logger = logger;            
            _report = report;
            _cartRepository = cartRepository;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                _recurringJobs.AddOrUpdate("Report", () => _report.CreateReport(), _setting.CronReport);
                _recurringJobs.AddOrUpdate("ClearCartsGreatMonth", () => _cartRepository.ClearCartsGreatMonth(), _setting.CronClear);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Task.CompletedTask;
        }
    }
}
