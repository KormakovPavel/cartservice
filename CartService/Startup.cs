using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Hangfire;
using Hangfire.SqlServer;

namespace CartService
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup()
        {
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            Configuration = builder.Build();
        }
       
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<Setting>(Configuration);
            services.TryAddSingleton(new SqlServerStorageOptions
            {
                CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                QueuePollInterval = TimeSpan.FromTicks(1),
                UseRecommendedIsolationLevel = true,
                SlidingInvisibilityTimeout = TimeSpan.FromMinutes(1)
            });
            services.AddHangfire((provider, configuration) => configuration
                           .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                           .UseSimpleAssemblyNameTypeSerializer()
                           .UseSqlServerStorage(Configuration["ConnectionHangfire"], provider.GetRequiredService<SqlServerStorageOptions>())
                       );
            services.AddHangfireServer(options =>
            {
                options.StopTimeout = TimeSpan.FromSeconds(15);
                options.ShutdownTimeout = TimeSpan.FromSeconds(30);
            });

            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<ICartRepository, CartRepository>();
            services.AddTransient<IWebhookRepository, WebhookRepository>();
            services.AddTransient<IWebhookPublisher, WebhookPublisher>();
            services.AddTransient<IReport, ExcelReport>();

            services.AddHostedService<RecurringJobsService>();

            services.AddControllers();

            services.AddSwaggerDocument(config =>
            {
                config.PostProcess = document =>
                {
                    document.Info.Title = "Cart API";
                    document.Info.Description = "�������� Web API ��� �������";
                    document.Info.Contact = new NSwag.OpenApiContact
                    {
                        Name = "�������� �����",
                        Email = "kormakov-11555@rambler.ru"                        
                    };
                };
            });
        }
               
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseOpenApi();
            app.UseSwaggerUi3();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
