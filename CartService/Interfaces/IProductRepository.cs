﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CartService
{
    public interface IProductRepository
    {
        List<string> ErrorMessages { get; set; }
        Task<bool> AddProduct(Product product);
        Task<bool> AddProducts(List<Product> products);
        Task<bool> DeleteProduct(int id);
        Task<bool> DeleteProducts(List<int> ids);
    }
}
