﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CartService
{
    public interface IWebhookRepository
    {
        List<string> ErrorMessages { get; set; }
        Task<IEnumerable<Webhook>> GetWebhooks();
        Task<bool> AddWebhook(Webhook webhook);
        Task<bool> DeleteWebhook(int id);
    }
}
