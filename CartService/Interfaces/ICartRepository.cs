﻿using System.Threading.Tasks;

namespace CartService
{
    public interface ICartRepository
    {
        Task<bool> ClearCartsGreatMonth();
        Task<int> GetCountCarts();
        Task<int> GetCountCartsWithProductBonus();
        Task<int> GetCountCartDeadLine(int countDay=0);
        Task<int> AVGCostCart();
    }
}
