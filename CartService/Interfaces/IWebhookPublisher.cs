﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CartService
{
    public interface IWebhookPublisher
    {
        List<string> ErrorMessages { get; set; }
        Task<bool> Send();
    }
}
