﻿using System.Threading.Tasks;

namespace CartService
{
    public interface IReport
    {
        Task<bool> CreateReport();
    }
}
