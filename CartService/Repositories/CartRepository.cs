﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace CartService
{
    public class CartRepository : Repository<int>, ICartRepository
    {
        private readonly IWebhookPublisher _webhookPublisher;
        public CartRepository(IOptions<Setting> options, IWebhookPublisher webhookPublisher) : 
            base(options.Value.ConnectionString)
        {
            _webhookPublisher = webhookPublisher;
        }
        public async Task<bool> ClearCartsGreatMonth()
        {
            var sqlComand = "DELETE FROM Carts WHERE Date_Time < DATEADD(DAY,-30,GETDATE())";

            var result = await ExecuteComand(sqlComand);
            if(!result)
            {
                return false;
            }

            return await _webhookPublisher.Send(); 
        }
        public async Task<int> GetCountCarts()
        {
            var sqlQuery = "SELECT COUNT(*) FROM Carts";
            var result = await ExecuteQuery(sqlQuery);

            return result.FirstOrDefault();           
        }

        public async Task<int> GetCountCartsWithProductBonus()
        {
            var sqlQuery = "SELECT COUNT(*) FROM (" +
                           "SELECT DISTINCT [IdCart] FROM " +
                           "(SELECT [IdCart] FROM [CartProducts] WHERE [ForBonusPoints]=1) AS A " +
                           ") AS B";
            var result = await ExecuteQuery(sqlQuery);

            return result.FirstOrDefault();            
        }
        public async Task<int> GetCountCartDeadLine(int countDay = 0)
        {
            var sqlQuery = $"SELECT COUNT(*) FROM Carts WHERE Date_Time BETWEEN DATEADD(DAY,-30,GETDATE()) AND DATEADD(DAY,-{countDay},GETDATE())";
            var result = await ExecuteQuery(sqlQuery);

            return result.FirstOrDefault();            
        }
        public async Task<int> AVGCostCart()
        {
            var sqlQuery = "SELECT " +
                           "(SELECT SUM(Cost) FROM [Products])/" +
                           "(SELECT COUNT(*) FROM Carts)";
            var result = await ExecuteQuery(sqlQuery);

            return result.FirstOrDefault();            
        }
    }
}
