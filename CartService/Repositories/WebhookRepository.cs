﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace CartService
{
    public class WebhookRepository : Repository<Webhook>, IWebhookRepository
    {
        public WebhookRepository(IOptions<Setting> options) :
           base(options.Value.ConnectionString)
        {
        }

        public async Task<IEnumerable<Webhook>> GetWebhooks()
        {
            var sqlQuery = "SELECT * FROM Webhooks";
            
            return await ExecuteQuery(sqlQuery);
        }

        public async Task<bool> AddWebhook(Webhook webhook)
        {
            if (webhook == null)
            {
                return false;
            }

            var sqlComand = "INSERT INTO Webhooks (Url, Content) VALUES(@Url, @Content)";

            return await ExecuteComand(sqlComand, webhook);
        }

        public async Task<bool> DeleteWebhook(int id)
        {
            var sqlComand = "DELETE FROM Webhooks WHERE Id = @Id";

            return await ExecuteComand(sqlComand, new { id });
        }
    }
}
