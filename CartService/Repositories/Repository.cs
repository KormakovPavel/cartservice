﻿using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;

namespace CartService
{
    public class Repository<T>
    {
        private readonly string _connectionString;
        public List<string> ErrorMessages { get; set; }
        public Repository(string connectionString)
        {
            _connectionString = connectionString;
            ErrorMessages = new List<string>();
        }
        public async Task<bool> ExecuteComand(string sqlComand,object param = null)
        {
            try
            {
                using IDbConnection sqlConnection = new SqlConnection(_connectionString);                
                await sqlConnection.ExecuteAsync(sqlComand, param);

                return true;
            }
            catch (SqlException ex)
            {
                ErrorMessages.Add(ex.Message);
                return false;
            }
        }

        public async Task<IEnumerable<T>> ExecuteQuery(string sqlQuery)
        {
            try
            {
                using IDbConnection sqlConnection = new SqlConnection(_connectionString);                
                var result = await sqlConnection.QueryAsync<T>(sqlQuery);

                return result;
            }
            catch (SqlException ex)
            {
                ErrorMessages.Add(ex.Message);
                return new List<T>();
            }
        }
    }
}
