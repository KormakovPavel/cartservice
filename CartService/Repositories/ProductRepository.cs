﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace CartService
{
    public class ProductRepository : Repository<object>, IProductRepository
    {       
        public ProductRepository(IOptions<Setting> options) : 
            base(options.Value.ConnectionString)
        {            
        }
        public async Task<bool> AddProduct(Product product)
        {
            if (product == null)
            {
                return false;
            }

            var sqlComand = "INSERT INTO Products (Name, Cost, ForBonusPoints, IdCart) VALUES(@Name, @Cost, @ForBonusPoints, @IdCart)";

            return await ExecuteComand(sqlComand, product);           
        }

        public async Task<bool> AddProducts(List<Product> products)
        {
            if (products == null)
            {
                return false;
            }

            foreach (var product in products)
            {
                var result = await AddProduct(product);
                if (!result)
                {
                    return false;
                }
            }

            return true;
        }

        public async Task<bool> DeleteProduct(int id)
        {
            var sqlComand = "DELETE FROM Products WHERE Id = @Id";

            return await ExecuteComand(sqlComand, new { id });          
        }

        public async Task<bool> DeleteProducts(List<int> ids)
        {          
            foreach (var id in ids)
            {
                var result = await DeleteProduct(id);
                if (!result)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
