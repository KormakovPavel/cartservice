﻿using System.Threading.Tasks;

namespace CartService
{
    public class Report
    {
        private readonly ICartRepository _cartRepository;      

        public Report(ICartRepository cartRepository)
        {
            _cartRepository = cartRepository;            
        }

        protected async Task<ReportData> GetReportData()
        {
            var countCarts = await _cartRepository.GetCountCarts();
            var countCartsWithProductBonus = await _cartRepository.GetCountCartsWithProductBonus();
            var countCartDeadLine10Day = await _cartRepository.GetCountCartDeadLine(20);
            var countCartDeadLine20Day = await _cartRepository.GetCountCartDeadLine(10);
            var countCartDeadLine30Day = await _cartRepository.GetCountCartDeadLine();
            var avgCostCart = await _cartRepository.AVGCostCart();

            return new ReportData()
            {
                CountCarts = countCarts,
                CountCartsWithProductBonus = countCartsWithProductBonus,
                CountCartDeadLine10Day = countCartDeadLine10Day,
                CountCartDeadLine20Day = countCartDeadLine20Day,
                CountCartDeadLine30Day = countCartDeadLine30Day,
                AvgCostCart = avgCostCart
            };
        }
    }
}
