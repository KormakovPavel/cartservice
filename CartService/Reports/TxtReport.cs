﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace CartService
{
    public class TxtReport : Report, IReport
    {
        private readonly Setting _setting;
        public TxtReport(ICartRepository cartRepository, IOptions<Setting> options) : 
            base(cartRepository)
        {
            _setting = options.Value;
        }

        public async Task<bool> CreateReport()
        {
            var reportData = await GetReportData();

            string content = $"Всего корзин: {reportData.CountCarts}\r\n" +
                             $"Cодержат продукты за баллы: {reportData.CountCartsWithProductBonus}\r\n" +
                             $"Истекут в течение 10/20/30 дней: {reportData.CountCartDeadLine10Day}/" +
                                                              $"{reportData.CountCartDeadLine20Day}/" +
                                                              $"{reportData.CountCartDeadLine30Day}\r\n" +
                             $"Cредний чек корзины: {reportData.AvgCostCart}";

            try
            {
                File.WriteAllText(Path.Combine(_setting.PathName, $"Report_{DateTime.Now.ToString("yyyyMMddhhmmss")}.txt"), content);
            }           
            catch(IOException)
            {
                return false;
            }

            return true;
        }
    }
}
