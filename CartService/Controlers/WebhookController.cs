﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CartService.Controllers
{
    [Route("api/[controller]")]
    public class WebhookController : Controller
    {
        private readonly IWebhookRepository _webhookRepository;        
        public WebhookController(IWebhookRepository webhookRepository)
        {
            _webhookRepository = webhookRepository;            
        }

        [HttpPost, Route("addwebhook")]
        public async Task<IActionResult> AddProducts([FromBody]Webhook webhook)
        {            
            if (!await _webhookRepository.AddWebhook(webhook))
            {
                return BadRequest(string.Join(";", _webhookRepository.ErrorMessages));
            }

            return Ok();
        }

        [HttpPost, Route("deletewebhook")]
        public async Task<IActionResult> DeleteRole([FromBody]int id)
        {
            if (!await _webhookRepository.DeleteWebhook(id))
            {
                return BadRequest(string.Join(";", _webhookRepository.ErrorMessages));
            }

            return Ok();
        }
    }
}
