﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CartService.Controllers
{
    [Route("api/[controller]")]
    public class CartController : Controller
    {
        private readonly IProductRepository _productRepository;        
        public CartController(IProductRepository productRepository)
        {
            _productRepository = productRepository;            
        }

        [HttpPost, Route("addproducts")]
        public async Task<IActionResult> AddProducts([FromBody]List<Product> products)
        {            
            if (!await _productRepository.AddProducts(products))
            {
                return BadRequest(string.Join(";", _productRepository.ErrorMessages));
            }

            return Ok();
        }

        [HttpPost, Route("deleteproducts")]
        public async Task<IActionResult> DeleteRole([FromBody]List<int> ids)
        {
            if (!await _productRepository.DeleteProducts(ids))
            {
                return BadRequest(string.Join(";", _productRepository.ErrorMessages));
            }

            return Ok();
        }
    }
}
