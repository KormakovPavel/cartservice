﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace CartService.Tests
{
    public class ProductRepositoryTests
    {
        private readonly IProductRepository _productRepository;
        public ProductRepositoryTests()
        {
            var dbContext = new DbContext();
            _productRepository = new ProductRepository(dbContext.Settings);           
        }
        [Fact]
        public async Task AddProductIsTrue()
        {
            var product = new Product()
            {
                Name = "Pizza",
                Cost = 200,
                ForBonusPoints = false,
                IdCart = 1
            };
            
            var result = await _productRepository.AddProduct(product);            
            Assert.True(result);
        }

        [Fact]
        public async Task AddProductsIsTrue()
        {
            var products = new List<Product>()
            {
                new Product()  {  Name = "Pizza",  Cost = 200,  ForBonusPoints = false,  IdCart = 1  },
                new Product()  {  Name = "Burger",  Cost = 300,  ForBonusPoints = false, IdCart = 1  }
            };

            var result = await _productRepository.AddProducts(products);
            Assert.True(result);
        }

        [Fact]
        public async Task AddProductNull()
        {
            var result = await _productRepository.AddProduct(null);
            Assert.False(result);
        }

        [Fact]
        public async Task AddProductsNull()
        {
            var result = await _productRepository.AddProducts(null);
            Assert.False(result);
        }

        [Fact]
        public async Task DeleteProduct()
        {           
            var result = await _productRepository.DeleteProduct(2);
            Assert.True(result);
        }

        [Fact]
        public async Task DeleteProducts()
        {
            var ids = new List<int> { 3, 4 };
            var result = await _productRepository.DeleteProducts(ids);
            Assert.True(result);
        }

    }
}
