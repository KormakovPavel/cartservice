﻿using System.Threading.Tasks;
using Xunit;

namespace CartService.Tests
{
    public class WebhookRepositoryTests
    {
        private readonly IWebhookRepository _webhookRepository;
        public WebhookRepositoryTests()
        {
            var dbContext = new DbContext();
            _webhookRepository = new WebhookRepository(dbContext.Settings);
        }

        [Fact]
        public async Task GetWebhooks()
        {            
            var result = await _webhookRepository.GetWebhooks();
            Assert.NotEmpty(result);
        }

        [Fact]
        public async Task AddWebhookIsTrue()
        {
            var webhook = new Webhook()
            {
                Url = "https://webhook.site/af821aac-9e5d-4596-ab5f-7e7332f500bd",
                Content = "{\"name\":\"John Doe\"}"                
            };

            var result = await _webhookRepository.AddWebhook(webhook);
            Assert.True(result);
        }

        [Fact]
        public async Task AddProductNull()
        {
            var result = await _webhookRepository.AddWebhook(null);
            Assert.False(result);
        }

        [Fact]
        public async Task DeleteProduct()
        {
            var result = await _webhookRepository.DeleteWebhook(2);
            Assert.True(result);
        }
    }
}
