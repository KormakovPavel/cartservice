﻿using System.Threading.Tasks;
using Xunit;

namespace CartService.Tests
{
    public class CartRepositoryTests
    {
        private readonly ICartRepository _cartRepository;        
        public CartRepositoryTests()
        {
            var dbContext = new DbContext();
            var webhookRepository = new WebhookRepository(dbContext.Settings);
            var webhookPublisher = new WebhookPublisher(webhookRepository);
            _cartRepository = new CartRepository(dbContext.Settings, webhookPublisher);
        }

        [Fact]
        public async Task ClearCartsGreatMonth()
        {
            var result = await _cartRepository.ClearCartsGreatMonth();
            Assert.True(result);
        }

        [Fact]
        public async Task GetCountCarts()
        {
            var result = await _cartRepository.GetCountCarts();
            Assert.Equal(3, result);
        }

        [Fact]
        public async Task GetCountCartsWithProductBonus()
        {
            var result = await _cartRepository.GetCountCartsWithProductBonus();
            Assert.NotEqual(2, result);
        }

        [Fact]
        public async Task GetCountCartDeadLine()
        {
            var result = await _cartRepository.GetCountCartDeadLine(10);
            Assert.NotEqual(0, result);
        }

        [Fact]
        public async Task AVGCostCart()
        {
            var result = await _cartRepository.AVGCostCart();
            Assert.Equal(400, result);
        }
    }
}
