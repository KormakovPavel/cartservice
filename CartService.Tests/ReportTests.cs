﻿using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Xunit;

namespace CartService.Tests
{
    public class ReportTests
    {        
        private readonly ICartRepository _cartRepository;
        private readonly IOptions<Setting> _settings;
        public ReportTests()
        {
            var dbContext = new DbContext();
            var webhookRepository = new WebhookRepository(dbContext.Settings);
            var webhookPublisher = new WebhookPublisher(webhookRepository);
            _cartRepository = new CartRepository(dbContext.Settings, webhookPublisher);
            _settings = dbContext.Settings;
        }

        [Fact]
        public async Task SaveTXTReport()
        {
            var report = new TxtReport(_cartRepository, _settings);
            var result = await report.CreateReport();
            Assert.True(result);
        }

        [Fact]
        public async Task SaveExcelReport()
        {
            var report = new ExcelReport(_cartRepository, _settings);
            var result = await report.CreateReport();
            Assert.True(result);
        }
    }
}
