﻿using System.Threading.Tasks;
using Xunit;

namespace CartService.Tests
{
    public class WebhookPublisherTests
    {
        private readonly IWebhookRepository _webhookRepository;
        private readonly IWebhookPublisher _webhookPublisher;
        public WebhookPublisherTests()
        {
            var dbContext = new DbContext();
            _webhookRepository = new WebhookRepository(dbContext.Settings);
            _webhookPublisher = new WebhookPublisher(_webhookRepository);
        }

        [Fact]
        public async Task SendIsTrue()
        {
            var result = await _webhookPublisher.Send();
            Assert.True(result);
        }
    }
}
