﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace CartService.Tests
{
    public class DbContext
    {
        private readonly IConfigurationRoot _configuration;
        public DbContext()
        {
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            _configuration = builder.Build();
        }
        public IOptions<Setting> Settings => Options.Create(_configuration.Get<Setting>());
        public string ConnectionString => _configuration["ConnectionString"];
        public string PathName => _configuration["PathName"];
    }
}
