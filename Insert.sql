USE [Shop]
GO

INSERT INTO [dbo].[Carts]
           ([Date_Time])
     VALUES
           ('08.06.2020 00:00:00')
GO
INSERT INTO [dbo].[Carts]
           ([Date_Time])
     VALUES
           ('07.30.2020 00:00:00')
GO
INSERT INTO [dbo].[Carts]
           ([Date_Time])
     VALUES
           ('07.21.2020 00:00:00')
GO

INSERT INTO [dbo].[Products]
           ([Name]
           ,[Cost]
           ,[ForBonusPoints]
           ,[IdCart])
     VALUES
           ('Pizza',
           200,
           1,
           1)
GO

INSERT INTO [dbo].[Products]
           ([Name]
           ,[Cost]
           ,[ForBonusPoints]
           ,[IdCart])
     VALUES
           ('Burger',
           300,
           0,
           1)
GO
INSERT INTO [dbo].[Products]
           ([Name]
           ,[Cost]
           ,[ForBonusPoints]
           ,[IdCart])
     VALUES
           ('Pizza',
           200,
           1,
           2)
GO

INSERT INTO [dbo].[Products]
           ([Name]
           ,[Cost]
           ,[ForBonusPoints]
           ,[IdCart])
     VALUES
           ('Burger',
           300,
           0,
           2)
GO
INSERT INTO [dbo].[Products]
           ([Name]
           ,[Cost]
           ,[ForBonusPoints]
           ,[IdCart])
     VALUES
           ('Burger',
           300,
           0,
           3)
GO


